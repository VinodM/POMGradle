package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.But;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead {
	
	public static ChromeDriver driver;
	
	@Given("launch the browser")
	public void launchBrowser() {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();		
	}
	
	@And("maximize the browser")
	public void maximize() {
		driver.manage().window().maximize();	
	}
	
	@And("set the timeouts")
    public void timeout() {
   
    	driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    	
    }
	
	@And("enter the URL as (.*)")
	public void URL(String URL) {
		
		driver.get(URL);
		
	}
	
	@And("enter the user name as (.*)")
	public void username(String user) {
		
		driver.findElementById("username").sendKeys(user);
	}
	
	@And("enter the password as (.*)")
	public void password(String pwd) {
		
		driver.findElementById("password").sendKeys(pwd);
	}
	
	@And("click on login")
	public void login() {
		
		driver.findElementByClassName("decorativeSubmit").click();
	}
	
	@And("click on CRMSFA")
	public void CRMlink() {
		
		driver.findElementByLinkText("CRM/SFA").click();
	}
	
	@And("click on Create Lead")
	public void Createlead(){
		
		driver.findElementByLinkText("Create Lead").click();
	}
	
	@And("enter the Company Name as (.*)")
	public void CompanyName(String cname) {
		
		driver.findElementById("createLeadForm_companyName").sendKeys(cname);
	}
	
	@And("Do not enter the Company Name")
	public void NoCompanyName() {
		
		driver.findElementById("createLeadForm_companyName");
	}
	
	@And("enter the first name as (.*)")
	public void FirstName(String fname) {
		
		driver.findElementById("createLeadForm_firstName").sendKeys(fname);
	}
	
	@And("enter the last name as (.*)")
	public void LastName(String lname) {
		
		driver.findElementById("createLeadForm_lastName").sendKeys(lname);
	}
	
	@When("clicks on Create Lead")
	public void createlead() {
		
		driver.findElementByClassName("smallSubmit").click();
	}
	
	@Then("Verify Create Lead is successful")
	public void verifyCreateLead() {
		
		driver.getTitle();
	}
	
	@But("Verify Create Lead is unsuccessful")
	public void createLeadfail() {
		
		String text = driver.findElementByXPath("//li[text()='The following required parameter is missing: [crmsfa.createLead.companyName]']").getText();
		System.out.println("Error: " + text);
	}
	
	
	
	
	
	
}
