package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{
	
	public MyLeadsPage() {
	
		PageFactory.initElements(driver, this);
		
	}

	
	public CreateLeadPage clickCreateLead() {
		WebElement eleCreateLead = locateElement("linktext", "Create Lead");
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	
	public MergeLeads clickMergeLead() {
		WebElement eleMergeLead = locateElement("linktext", "Merge Leads");
		click(eleMergeLead);
		return new MergeLeads();
	}
	
	
	
	@FindBy(linkText = "Find Leads")
	WebElement eleFindLeads;
	
	public FindLeadsPage clickFindLeads() {
		
		//WebElement eleFindLeads = locateElement("linktext", "Find Leads");
		click(eleFindLeads);
		return new FindLeadsPage();
		
	}
	
}









