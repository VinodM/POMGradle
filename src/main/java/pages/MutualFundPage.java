package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import wdMethods.ProjectMethods;

public class MutualFundPage extends ProjectMethods {

	public MutualFundPage searchMutualFund() {

		WebElement search = locateElement("linktext", "Search for Mutual Funds");
		click(search);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;

	}

	public MutualFundPage selectAge() {

		int age = Integer.parseInt("32");
		Actions action = new Actions(driver);
		WebElement slider = driver.findElementByXPath("//div[@class='rangeslider__handle']");
		action.dragAndDropBy(slider, (age-18)*8, 0).perform();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;

	}


	public MutualFundPage selectMonth() {

		WebElement month = locateElement("linktext", "May 1986");
		click(month);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;	
	}


	public MutualFundPage selectDate() {
		
		WebElement date = locateElement("xpath", "//div[text()='25']");
		click(date);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}
	
	
	public MutualFundPage verifyDOB() {

     WebElement DOB = locateElement("xpath", "//div[@class='Calendar_tableHeading_1ny8Y Calendar_yearLabel_3-jJc']");
     String text = DOB.getText();
     System.out.println(text);
     return this;
	}
	
	
	public MutualFundPage clickContinue() {

	 WebElement next = locateElement("linktext", "Continue");
	 click(next);
	 try {
		Thread.sleep(3000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 return this;
	 

	}


	public MutualFundPage selectSalary() {
		
		WebElement salary = locateElement("name", "netAnnualIncome");
		salary.sendKeys("750000");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
		
	}
	
	public MutualFundPage clickNext() {
		
		WebElement next1 = locateElement("linktext", "Continue");
		next1.click();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}
	
	
	public MutualFundPage selectBank() {
		
		WebElement bank = locateElement("xpath", "//span[text()='ICICI']");
		bank.click();
		return this;
	}
	
	public MutualFundPage typeName(){
		
		WebElement fname = locateElement("name", "firstName");
		fname.sendKeys("Vinod");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}
	
	public MutualFundPage clickMutualFunds() {
		
		WebElement mutualfund = locateElement("linktext", "View Mutual Funds");
		mutualfund.click();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}
	
	public MutualFundPage alertClose() {
		
		driver.switchTo().alert().dismiss();
		return this;
		
	}
	
	
	public void schemeName() {
		
		List<WebElement> scheme = driver.findElementsByClassName("js-offer-name");
		for (WebElement eachScheme : scheme) {
		 String text = eachScheme.getText();
		 System.out.println(text);
		 WebElement amount = locateElement("xpath", "//li[@class='js-offer-name']/following::span[@class='fui-rupee bb-rupee-xs']/..");
		 String text2 = amount.getText();
		 System.out.println(text2);
		}
		
	}
	
}
