package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods {
	
			
		public FindLeadsPage typeFirstName(String data) {
		WebElement eleFirstName = locateElement("xpath", "//div[@class='x-form-item x-tab-item']/div[@class='x-form-element']/input[@name='firstName']");
		type(eleFirstName, data);
		return this;
		}
		
		public FindLeadsPage clickFindLeads() throws InterruptedException{
		WebElement eleFindLeads = locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeads);
		Thread.sleep(3000);
		return this;
		}
		
		
		public ViewLeadsPage clickSearch() throws InterruptedException {
		
		WebElement eleSearch = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
		click(eleSearch);
		return new ViewLeadsPage();
		}
		
		public MergeLeads clickMergeSearch() throws InterruptedException {
			
			WebElement eleSearch = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
			click(eleSearch);
			return new MergeLeads();
			}
		
		public FindLeadsPage clickEmail() {
			
			WebElement eleEmail = locateElement("xpath", "//span[text()='Email']");
			click(eleEmail);
			return this;
		}
		
		public FindLeadsPage typeEmail(String data) {
			
			WebElement eleemail = locateElement("name", "emailAddress");
			type(eleemail, data);
			return this;
		}
		
		
		
		
		
		
		/*public void verifytext() {
			
			verifyExactText(ele, expectedText);
			
		}
		*/

		
		
	}


