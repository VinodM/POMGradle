package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class ViewLeadsPage extends ProjectMethods{

	public EditLeads clickeditLead() {
		WebElement eleEditLead = locateElement("linktext", "Edit");
		click(eleEditLead);
		return new EditLeads();
		
	}
	
	
	/*public void verifyupdate() {
		
		WebElement fname = locateElement("id", "viewLead_firstName_sp");
		String text = fname.getText();
		
		text.equalsIgnoreCase();
		
		
	}*/
	
	public ViewLeadsPage typeleadid(String data) {
		WebElement eleLeadID = locateElement("xpath", "//input[@name='id']");
		
		type(eleLeadID, data);
		return this;
	}
	
	
	public ViewLeadsPage clickfindleads() {
		
		WebElement elefindleads = locateElement("linktext", "Find Leads");
		click(elefindleads);
		return new ViewLeadsPage(); 
		
	}
	
	public MyLeadsPage clickDelete() {
		
		WebElement eleDelete = locateElement("linktext", "Delete");
		click(eleDelete);
		return new MyLeadsPage();
		
	}
	
	public DuplicateLeadPage clickDuplicate() throws InterruptedException {
		
		WebElement eleDuplicate = locateElement("linktext", "Duplicate Lead");
		click(eleDuplicate);
		Thread.sleep(2000);
		return new DuplicateLeadPage();
	}
	
	
	/*public DuplicateLeadPage getDuplicateTitle() {
		
		WebElement eleDuplicate1 = locateElement("linktext", "Duplicate Lead");
		String eleDuplicatetitle = getTitle();
		return new DuplicateLeadPage();
	}
	*/
	
	
}









