package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class DuplicateLeadPage extends ProjectMethods {
	
	
	
	public ViewLeadsPage clickCreateLead() {
		
		WebElement eleCreateLead = locateElement("xpath", "//input[@name='submitButton']");
		click(eleCreateLead);
		return new ViewLeadsPage();
		
	}

}
