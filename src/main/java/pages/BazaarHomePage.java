package pages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import wdMethods.ProjectMethods;

public class BazaarHomePage extends ProjectMethods {
	
	
	public MutualFundPage mouseHoverInvestments()  {
		
		Actions action = new Actions(driver);
		WebElement hover = driver.findElementByLinkText("INVESTMENTS");
		action.moveToElement(hover).perform();
		WebElement MutualFund = driver.findElementByLinkText("Mutual Funds");
		click(MutualFund);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new MutualFundPage();
		
		
	}
	
	
	
	

}
