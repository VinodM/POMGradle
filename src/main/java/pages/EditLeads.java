package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class EditLeads extends ProjectMethods{

	public EditLeads typeCompanyName(String data) {
		WebElement eleCompanyName = locateElement("id", "updateLeadForm_companyName");
		
		type(eleCompanyName, data);
		return this;
	}
	
	public EditLeads typeFirstName(String data) {
		WebElement eleFirstName = locateElement("id", "updateLeadForm_firstName");
		
		type(eleFirstName, data);
		return this;
	}
	public EditLeads typeLastName(String data) {
		WebElement eleLastName = locateElement("id", "updateLeadForm_lastName");
		
		type(eleLastName, data);
		return this;
	}
	
	public ViewLeadsPage clickUpdate() {
		WebElement eleUpdateLead= locateElement("class", "smallSubmit");
		click(eleUpdateLead);
		return new ViewLeadsPage(); 
	}
	
}









