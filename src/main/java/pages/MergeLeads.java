package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class MergeLeads extends ProjectMethods {


	public FindLeadsPage ClickFromLead() {
		WebElement ClickFromLead = locateElement("xpath", "(//img[@alt='Lookup'])[1]");
		click(ClickFromLead);
		switchToWindow(1);
		return new FindLeadsPage();
	}

	
	public FindLeadsPage ClickToLead() {
		switchToWindow(0);
		WebElement ClickToLead = locateElement("xpath", "(//img[@alt='Lookup'])[2]");
		click(ClickToLead);
		switchToWindow(1);
		return new FindLeadsPage();
	}  
	

	public MergeLeads clickMerge() {
		switchToWindow(0);
		WebElement eleMerge = locateElement("linktext", "Merge");
		click(eleMerge);
		return this;
	}
	
	public ViewLeadsPage alert() {
		
		acceptAlert();
		return new ViewLeadsPage();
	}



}


