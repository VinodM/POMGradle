package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC002_EditLead extends ProjectMethods {
	
	@BeforeClass(groups="common")
	public void setData() {
		testCaseName = "TC002_EditLead";
		testCaseDescription ="Edit a lead";
		category = "Smoke";
		author= "Babu";
		dataSheetName="TC002";
	}
	
	
	@Test(dataProvider="fetchData")
	public  void editLead(String fname, String updateCname, String updatefname, String updatelname)  throws InterruptedException {
		
		new MyHomePage()
		.clickLeads()
		.clickFindLeads()
		.typeFirstName(fname)
		.clickFindLeads()
		.clickSearch()
		.clickeditLead()
		.typeCompanyName(updateCname)
		.typeFirstName(updatefname)
		.typeLastName(updatelname)
		.clickUpdate();
		
		
		
	}

	
	
	

}
