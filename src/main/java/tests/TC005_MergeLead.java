package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC005_MergeLead extends ProjectMethods{
	
	@BeforeClass
	public void setData() {
		testCaseName = "TC005_MergeLead";
		testCaseDescription ="Merge two leads";
		category = "Smoke";
		author= "Babu";
		dataSheetName="TC005";
	}
	@Test(dataProvider="fetchData")
	public  void mergeLead(String fromLead, String toLead) throws InterruptedException   {
	
		new MyHomePage()
		.clickLeads()
		.clickMergeLead()
		.ClickFromLead()
		.typeFirstName(fromLead)
		.clickFindLeads()
		.clickMergeSearch()
		.ClickToLead()
		.typeFirstName(toLead)
		.clickFindLeads()
		.clickMergeSearch()
		.clickMerge()
		.alert();
		
		
		
	}

}
