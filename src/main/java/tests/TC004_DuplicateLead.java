package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC004_DuplicateLead extends ProjectMethods {
	
	@BeforeClass(groups="common")
	public void setData() {
		testCaseName = "TC004_DuplicateLead";
		testCaseDescription ="Duplicate a lead";
		category = "Smoke";
		author= "Babu";
		dataSheetName="TC004";
	}
	
	
	@Test(dataProvider="fetchData")
	public  void duplicateLead(String email, String title)  throws InterruptedException {
		
		new MyHomePage()
		.clickLeads()
		.clickFindLeads()
		.clickEmail()
		.typeEmail(email)
		.clickFindLeads()
		.clickSearch()
		.clickDuplicate()
		.clickCreateLead();
		
		
		
		
		
	}

	
	
	

}
