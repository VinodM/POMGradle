package testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.BazaarHomePage;
import wdMethods.ProjectMethods;

public class BankBazaar extends ProjectMethods {
	
	@BeforeClass
	public void setData() {
		testCaseName = "BankBazaar";
		testCaseDescription ="Mutual Fund";
		category = "Smoke";
		author= "Vinod";
		dataSheetName="TC006";
	}
	
	@Test(dataProvider="fetchData")
	public  void bankBazaar()   {
		
		new BazaarHomePage()
		.mouseHoverInvestments()
		.searchMutualFund()
		.selectAge()
		.selectMonth()
		.selectDate()
		.verifyDOB()
		.clickContinue()
		.selectSalary()
		.clickNext()
		.selectBank()
		.typeName()
		.clickMutualFunds()
		.schemeName();
		
		

}
	
}
